import React, {useState, memo} from 'react';

import Card from '../UI/Card';
import LoadingIndicator from '../UI/LoadingIndicator';
import './IngredientForm.css';

const IngredientForm = props => {
  const initialIngredientState = {title: '', amount: ''};
  const [ingredient, setIngredient] = useState(initialIngredientState);
  // const inputRef = useRef();

  const changeHandler = event => {
    const id = event.target.id;
    const value = event.target.value;

    // console.log(inputRef.current.value===value);

    setIngredient(prevIngredients=>({
      ...prevIngredients,
      [id]:value
    }));
  };

  const submitHandler = event => {
    event.preventDefault();

    ingredient !== initialIngredientState && props.addIngredient(ingredient);

    setIngredient(initialIngredientState);
  };

  // console.log('render IngredientForm');
  return (
    <section className="ingredient-form">
      <Card>
        <form onSubmit={submitHandler}>
          <div className="form-control">
            <label htmlFor="title">Name</label>
            <input
              // ref={inputRef}
              type="text"
              id="title"
              value={ingredient.title}
              onChange={changeHandler}
              />
          </div>
          <div className="form-control">
            <label htmlFor="amount">Amount</label>
            <input type="number" id="amount" value={ingredient.amount} onChange={changeHandler}/>
          </div>
          <div className="ingredient-form__actions">
            <button type="submit">Add Ingredient</button>
            {props.loading && <LoadingIndicator />}
          </div>
        </form>
      </Card>
    </section>
  );
};

export default memo(IngredientForm);
