import React, {memo} from 'react';

import './IngredientList.css';

const IngredientList = props => {
  console.log('render IngredientList');
  return (
    <section className="ingredient-list">
      <h2>Loaded Ingredients</h2>
      <ul>
        {props.ingredients.map(ig => (
          <li key={ig.id} onClick={() => props.onRemoveItem(ig.id)}>
            <span>{ig.title}</span>
            <span>{ig.amount} x</span>
          </li>
        ))}
      </ul>
    </section>
  );
};

function areEqual(prevProps, nextProps) {
    return prevProps.ingredients === nextProps.ingredients;
}

export default memo(IngredientList, areEqual);
