import React, {useEffect, useCallback, useReducer, useState} from 'react';

import IngredientForm from './IngredientForm';
import IngredientList from './IngredientList';
import Search from './Search';

import ErrorModal from '../UI/ErrorModal';

import {ingredientReducer, ingredientInitReducer} from '../../context/reducer/ingredient';
// import {httpReducer, httpInitReducer} from '../../context/reducer/http';
import {action} from '../../util/utility';
import useHttp from '../../hooks/http';

function Ingredients() {
  // const [ingredients, setIngredients] = useState([]);
  // const [searchFilter, setSearchFilter] = useState('');
  // const [filteredIngredients, setFilteredIngredients] = useState([]);
	// const [httpr.isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState();

	const [igd, dispatch] = useReducer(ingredientReducer, ingredientInitReducer);
	// const [httpr, dispatchHttpr] = useReducer(httpReducer, httpInitReducer);

	const {isLoading, sendRequest} = useHttp();

  const addIngredient = useCallback(ingredient => {
		// setIsLoading(true);
		// dispatchHttpr(action('START'));
    // fetch('https://react-hooks-update-adfa8.firebaseio.com/ingredients.json',{
    //   method: 'POST',
    //   body: JSON.stringify(ingredient),
    //   headers: {'Content-Type':'application/json'}
    // }).then(res=>res.json())
    // .then(resp => {
		// 	// setIsLoading(false);
		// 	dispatchHttpr(action('SUCCESS'));
    //   // setIngredients([...ingredients, {id: resp.name, ...ingredient}]);
		// 	dispatch(action('ADD', {ingredient: {id: resp.name,...ingredient}}));
    // });

		sendRequest(
			{
				url: 'https://react-hooks-update-adfa8.firebaseio.com/ingredients.json',
				method: 'POST',
				body: JSON.stringify(ingredient)
			}
		).then(resp=>{
					dispatch(action('ADD', {ingredient: {id: resp.name,...ingredient}}));
			});

  },[sendRequest]);

  const onRemoveItem = useCallback((id) => {
		// // setIsLoading(true);
		// dispatchHttpr(action('START'));
		//
		// fetch(`https://react-hooks-update-adfa8.firebaseio.com/ingredients/${id}.json`,{
		// 	method: 'DELETE'
		// }).then(res=>{
		// 	// setIsLoading(false);
		// 	dispatchHttpr(action('SUCCESS'));
		// 	// setIngredients( prevIngredients => prevIngredients.filter(ig=> ig.id!==id) );
		// 	dispatch(action('DELETE',{id}));
		// }).catch(err=>{
		// 	setError('Error while deleting');
		// 	// setIsLoading(false);
		// 	dispatchHttpr(action('ERROR'));
		// });

		sendRequest(
			{
				url: `https://react-hooks-update-adfa8.firebaseio.com/ingredients/${id}.json`,
				method: 'DELETE'
			}
		)
		.then(res=>{
			console.log('success');

				dispatch(action('DELETE',{id}));
			}
		)
		.catch(err=>{
			console.log('error');
			setError('Error while deleting');
		});
  },[sendRequest]);

  useEffect(()=>{
		// console.log('fetch ingredients effect');
    // fetch('https://react-hooks-update-adfa8.firebaseio.com/ingredients.json')
    //   .then(res => res.json())
    //   .then(res => {
    //     const loadedIngredients = [];
    //     for (let key in res){
    //       loadedIngredients.push({
    //         id: key,
    //         title: res[key].title,
    //         amount: res[key].amount
    //       });
    //     }
    //     // setIngredients(loadedIngredients);
		// 		dispatch(action('SET',{ingredients:loadedIngredients}));
    //   });
			sendRequest(
				{
					url: `https://react-hooks-update-adfa8.firebaseio.com/ingredients.json`
				},
			)
			.then(res=>{
				const loadedIngredients = [];
		        for (let key in res){
		          loadedIngredients.push({
		            id: key,
		            title: res[key].title,
		            amount: res[key].amount
		          });
		        }
					dispatch(action('SET',{ingredients:loadedIngredients}));
				})
			.catch(err=>{
					setError('Error while fetching');
				});
  },[sendRequest]);

  const searchChangeHandler = useCallback(e =>{
    // setSearchFilter(e.target.value);
		dispatch(action('SET_SEARCH_FILTER', {searchFilter:e.target.value}));
  },[]);

  useEffect(()=>{
		// console.log('filter effect');
    // setFilteredIngredients(igd.ingredients.filter(igd=>igd.title.includes(searchFilter)));
		dispatch(action('SEARCH', {searchFilter: igd.searchFilter}))
  },[igd.searchFilter]);

	const onCloseModal = () => {
		setError(null);
	}

  return (
    <div className="App">
			{error && <ErrorModal onClose={onCloseModal}>{error}</ErrorModal>}

      <IngredientForm addIngredient={addIngredient} loading={isLoading}/>

      <section>
        <Search onChange={searchChangeHandler} searchValue={igd.searchFilter}/>
        {igd.ingredients.length>0?
						<IngredientList
							ingredients={igd.searchFilter.length>0?igd.filteredIngredients:igd.ingredients}
							onRemoveItem={onRemoveItem}/>
					:null}
      </section>
    </div>
  );
}

export default Ingredients;
