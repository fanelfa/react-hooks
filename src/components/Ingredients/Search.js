import React, {memo} from 'react';

import Card from '../UI/Card';
import './Search.css';

const Search = props => {
  console.log('render Search');
  return (
    <section className="search">
      <Card>
        <div className="search-input">
          <label>Filter by Title</label>
          <input type="text" onChange={props.onChange} value={props.searchValue}/>
        </div>
      </Card>
    </section>
  );
};

export default memo(Search);
