export const ingredientInitReducer = {
	ingredients: [],
	searchFilter: '',
	filteredIngredients: [],
	// isLoading: false,
	// error: null
}

export const ingredientReducer = (state, action) => {
	switch (action.type){
		case 'SET':
      return {
        ...state,
        ingredients: action.payload.ingredients
      };
    case 'ADD':
      return {
        ...state,
        ingredients: [...state.ingredients, action.payload.ingredient]
      }
    case 'DELETE':
      return {
        ...state,
        ingredients: [...state.ingredients.filter(igd=>igd.id!==action.payload.id)]
      }
    case 'SET_SEARCH_FILTER':
      return {
        ...state,
        searchFilter: action.payload.searchFilter
      }
    case 'SEARCH':
      return {
        ...state,
        filteredIngredients: [...state.ingredients.filter(igd=>igd.title.includes(action.payload.searchFilter))]
      }
    default:
      throw new Error('Should not get there!');
	}
};
