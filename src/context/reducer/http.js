export const httpInitReducer = {
  isLoading: false,
  isError: false,
  isSuccess: false,
  data: null,
  error: null
}

export const httpReducer = (state, action) => {
	switch (action.type){
		case 'START':
      return {
        ...httpInitReducer,
        isLoading: true
      };
    case 'SUCCESS':
      return {
        ...httpInitReducer,
        isSuccess: true,
        data: action.payload.data
      };
    case 'ERROR':
      return {
        ...httpInitReducer,
        isError: true,
        error: action.payload.error
      };
    case 'RESET':
      return httpInitReducer;
    default:
      throw new Error('Should not get there!');
  }
}
