import {useReducer, useCallback} from 'react';

import {httpReducer, httpInitReducer} from '../context/reducer/http';

import {action} from '../util/utility';


const useHttp = () => {
  const [httpr, dispatch] = useReducer(httpReducer, httpInitReducer);

  const sendRequest = useCallback(({url, method, body}, successFunction, errorFunction) => {
    dispatch(action('START'));
    return fetch(
      url,
      {
        method: method,
        body: body,
        headers: {'Content-Type':'application/json'}
      }
    )
    .then(res=>res.json())
    .then(resp => {
      dispatch(action('SUCCESS', {data: resp}));
      // successFunction(resp);
      return resp;
    })
    .catch(err=>{
      dispatch(action('ERROR', {error: err}));
      // errorFunction(err);
      return err;
    });
  },[]);

  return {
    isLoading: httpr.isLoading,
    data: httpr.data,
    error: httpr.error,
    isSuccess: httpr.isSuccess,
    isError: httpr.isError,
    sendRequest: sendRequest
  }
}

export default useHttp;
